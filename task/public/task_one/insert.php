<?php

require_once 'config/db.php';
//Задача номер 1 парсинг, и заливка данных в БД
$con = connectToDB();
$xml = simplexml_load_file('russian_names.xml');
$index = 1;
$totalStudents = $xml->xpath("//Surnames/russian_names");
$total = count($totalStudents);

foreach ($totalStudents as $value) {
    progressBar($index, $total,50);
    $sql_insert = "INSERT INTO student VALUES("
        . (int)$value->ID .", '"
        . (string)$value->Name . "', '"
        . (string)$value->Sex . "')";
    getInsert($con, $sql_insert);
    $index++;
}
echo "\nДанные успешно залиты";

function progressBar($current = 0, $total = 100, $size = 50)
{
    $perc = intval(($current/$total)*100);
    for($i=strlen($perc); $i <= 4; $i++) {
        $perc = ' '. $perc;
    }
        $total_size = $size + $i + 3;

    if($current > 1)
    {
        for($place = $total_size; $place > 0; $place--)
        {
            echo "\10";
        }

        for($place = 0; $place <= $size; $place++)
        {
            if($place <= $current / $total * $size)
                echo '#';
            else
                echo '*';
        }
    } elseif ($current == $total) {
        echo "\n";
    }
    echo " $perc%";
}
?>





