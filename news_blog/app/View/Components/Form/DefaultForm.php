<?php

namespace App\View\Components\Form;

use App\View\Components\AbstractConstructor;
use App\View\Components\Elements\Button;
use App\View\Components\Elements\Color;
use App\View\Components\Elements\Icon;

class DefaultForm extends AbstractConstructor
{
    const BUTTON_APPLY = 'save';

    const BUTTON_BACK = 'back';

    protected $name = 'form';

    private $action = '';

    private $submitButton = [];

    private $fields = [];

    public function setButtons($buttons)
    {
        foreach ($buttons as $button) {
            $submitButton = $this->makeButton($button);
            $this->submitButton[] = $submitButton;
        }
        return $this;
    }

    public function getViewForm()
    {
        return [
            'submitButton' => $this->submitButton,
            'title' => $this->title,
            'icon'  => $this->icon,
            'action' => $this->action,
            'fields' => $this->fields,
        ];
    }

    private function makeButton($button)
    {
        $type = $button['type'];

        switch ($type) {
            case self::BUTTON_APPLY:
                $newButton = new Button('Сохранить', $button['action'], self::BUTTON_APPLY);
                $newButton->setColor(Color::GREEN_LIGHTER)->setPlace('right')->setIcon(Icon::SAVE);
                return $newButton;
            case self::BUTTON_BACK:
                $newButton = new Button('Назад', $button['action'],self::BUTTON_BACK);
                $newButton->setColor(Color::GREY)->setIcon(Icon::BACK);
                return $newButton;
            default:
                return new \Exception("Не существует такой кнопки {$type}");
        }
    }

    public function setAction($action = '')
    {
        return $this->action = $action;
    }

    public function addField($idField = '', $paramField = [])
    {
        switch ($paramField['type']) {
            case 'text' :
                $this->fields[$idField] = [
                    'type' => $paramField['type'],
                    'title' => $paramField['title'],
                    'value' => !empty($paramField['value']) ? $paramField['value'] : null,
                ];
                break;
            case  'textarea':
                $this->fields[$idField] = [
                    'type' => $paramField['type'],
                    'title' => $paramField['title'],
                    'value' => !empty($paramField['value']) ? $paramField['value'] : null,
                ];
                break;
            case 'multiple':
                $this->fields[$idField] = [
                    'type' => 'multiple',
                    'title' => $paramField['title'],
                    'items' => !empty($paramField['items']) ? $paramField['items'] : [],
                    'value' => !empty($paramField['value']) ? $paramField['value'] : [],
                ];
                break;
        }
        $this->fields[$idField]['name'] = $idField;
        $this->fields[$idField]['id'] = $idField . "_". uniqid();
        return $this;
    }
}