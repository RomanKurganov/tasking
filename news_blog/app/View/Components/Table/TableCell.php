<?php

namespace App\View\Components\Table;

class TableCell
{
    /** @var string */
    private $value = '';

    public function __construct($value)
    {
        $this->value = $value;
    }

    public function getValue()
    {
        return $this->value;
    }
}