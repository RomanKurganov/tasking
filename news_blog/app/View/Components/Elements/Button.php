<?php

namespace App\View\Components\Elements;

class Button
{
    protected $text;

    protected $action;

    protected $icon = '';

    protected $color = Color::BLUE;

    protected $place = '';

    private $type = '';

    public function __construct($text, $action, $type)
    {
        $this->text = $text;
        $this->action = $action;
        $this->type = $type;
    }

    public function setColor($color = '')
    {
        $this->color = $color;
        return $this;
    }

    public function getAction()
    {
        return url($this->action);
    }

    public function getText()
    {
        return $this->text;
    }

    public function setIcon($icon = '')
    {
        $this->icon = $icon;
        return $this;
    }

    public function setPlace($place = '')
    {
        $this->place = $place;
        return $this;
    }

    public function getClass()
    {
        return "waves-effect waves-light {$this->color} {$this->place} btn";
    }

    public function getIcon()
    {
        return $this->icon;
    }

    public function hasIcon()
    {
        return !empty($this->icon);
    }

    public function getType()
    {
        return $this->type;
    }

    public function getViewButton()
    {
        return view('common.elements.button', ['button' => $this]);
    }
}
