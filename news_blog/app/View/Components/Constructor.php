<?php

namespace App\View\Components;

use App\View\Components\Filter\DefaultFilter;
use App\View\Components\Form\DefaultForm;
use App\View\Components\Table\DefaultTable;
use Symfony\Component\Process\Exception\LogicException;

class Constructor extends AbstractConstructor
{
    public static function createElement($element)
    {
        switch ($element) {
            case 'table':
                return new DefaultTable();
            break;
            case 'filter':
                return new DefaultFilter();
            break;
            case 'form':
                return new DefaultForm();
            break;
            default:
                return new LogicException('Not Found Constructor: `'. $element.'`');
        }

    }

}