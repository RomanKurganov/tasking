<?php

namespace App\View\Components\Filter;

use App\View\Components\AbstractConstructor;

class DefaultFilter extends AbstractConstructor
{
    protected $name = 'filter';

    private $fields = [];
    
    protected $isFilter = false;

    protected $action = '';

    public function setAction($action = '')
    {
        return $this->action = $action;
    }

    public function getViewFilter()
    {
        return [
            'title' => $this->title,
            'icon'  => $this->icon,
            'fields' => $this->fields,
            'action'  => $this->action,
        ];
    }

    public function addField($idField = '', $paramField = [])
    {
        switch ($paramField['type']) {
            case 'text' :
                $this->fields[$idField] = [
                    'type' => $paramField['type'],
                    'title' => $paramField['title'],
                    'value' => !empty($paramField['value']) ? $paramField['value'] : null,
                ];
                break;
            case  'textarea':
                $this->fields[$idField] = [
                    'type' => $paramField['type'],
                    'title' => $paramField['title'],
                    'value' => !empty($paramField['value']) ? $paramField['value'] : null,
                ];
                break;
            case 'chips':
                $this->fields[$idField] = [
                    'type' => $paramField['type'],
                    'title' => $paramField['title'],
                    'items' => !empty($paramField['items']) ? $paramField['items'] : [],
                    'value' => !empty($paramField['value']) ? $paramField['value'] : [],
                ];
                break;
        }
        $this->fields[$idField]['name'] = $idField;
        $this->fields[$idField]['id'] = $idField . "_". uniqid();

        return $this;
    }

    public function isFilter()
    {
        $request = \Request::input();
        if (!empty($request)) {
            foreach (array_keys($request) as $value) {
               if(is_array($request[$value])) {

                   $this->fields[$value]['value'] = implode(", ", $request[$value]);
               }
                $this->fields[$value]['value'] = $request[$value];
            }
            return true;
        }
        return false;
    }

    public function filterField($field)
    {
        $column = $this->fields[$field]['value'];
        return $column;
    }
}
