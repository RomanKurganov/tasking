<?php

namespace App\Models\NewsBlog;

/**
 * Trait NewsScopeTrait
 * @package App\Models\NewsBlog
 */
trait NewsScopeTrait
{
    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param $value
     * @return $query
     */
    public function scopeTitle($query, $value)
    {
        if (!is_null($value)) {
           $query->where('title_news', 'ILIKE', "%{$value}%");
        }

    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $query

     * @param array $values
     * return $query
     */
    public function scopeTags($query, $values = [])
    {
        if (is_array($values) && !empty($values)) {
            $query->leftJoin('tags_rel_news', 'tags_rel_news.news_id','=','news_blog.id')
                  ->leftJoin('tags_blog', 'tags_blog.id', '=', 'tags_rel_news.tags_id')
                  ->whereIn('tags_blog.title_tag', $values);

        }


    }
}
