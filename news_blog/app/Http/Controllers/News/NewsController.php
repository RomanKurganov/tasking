<?php

namespace App\Http\Controllers\News;

use App\Models\NewsBlog\News;
use App\Service\NewsBlogService;
use App\View\Components\Table\DefaultTable;
use App\View\UIView\NewsUI;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * Class NewsController
 * @package App\Http\Controllers\News
 */
class NewsController extends Controller
{
    private $viewNews;
    private $newsBlogService;

    public function __construct(Request $request, NewsBlogService $newsBlogService)
    {
        $this->viewNews = new NewsUI($request);
        $this->newsBlogService = $newsBlogService;
    }

    public function index(Request $request)
    {
        $this->viewNews->initList();
        $perPage = $this->viewNews->getTable()->setPerPage();
        $query = $this->newsBlogService->getNewsBlogQuery();

        $rows = $this->viewNews->filter($query)->paginate($perPage);
        $rows = $rows->appends(['perPage' => $perPage]);
        $this->viewNews->getTable()->setPagination($rows->render('pagination.pagination'));
        $index = $rows->firstItem();

        foreach ($rows as $row) {
            $item = $row->toArray();
            $tags = [];
            foreach ($item['tags'] as $value) {
                $tags[] = $value['title_tag'];
            }
            $this->viewNews->getTable()->addRow([
                'index' => $index++,
                'title_news' => $item['title_news'],
                'text_news' => $item['text_news'],
                'tags' => implode(', ', array_filter($tags)),
                'created_at' => $item['updated_at'],
            ],[
                [
                    'type' => DefaultTable::BUTTON_EDIT,
                    'href' => route('news.edit', ['id' => $row->id]),
                ],
                [
                    'type' => DefaultTable::BUTTON_DELETE,
                    'href' => route('news.delete', ['id' => $row->id]),
                ],
            ]);
        }
        if ($request->ajax()) {
            return response()->json($this->viewNews->ajax()->render(), '200');
        }
        return $this->viewNews->getViewList();
    }

    public function add()
    {
        $newsBlog = new News();
        if ($this->newsBlogService->saveNewsBlog($newsBlog)) {
            return redirect()->route('news.edit', ['id' => $newsBlog->id]);
        };
        return redirect()->route('news.list')->withInput();
    }

    public function store(Request $request)
    {
        $news = $this->newsBlogService->getNewsBlogById($request->id);

        if ($this->newsBlogService->saveNewsBlog($news, $request->all())) {
            \Session::flash('message', 'Запись успешно сохранена');
            return redirect()->route('news.edit', ['id' => $request->id]);
        };
        return redirect()->route('news.list')->withInput();
    }

    public function edit(Request $request)
    {
        $news = $this->newsBlogService->getNewsBlogById($request->id);
        $tags = $this->newsBlogService->getAllTags();
        $this->viewNews->initForm($news, $tags);
        return $this->viewNews->getViewNews();
    }

    public function delete(Request $request)
    {
        $news = $this->newsBlogService->getNewsBlogById($request->id);
        $this->newsBlogService->deleteNews($news);
        \Session::flash('message', 'Запись успешно удалена!');
        return redirect()->route('news.list');
    }
}
