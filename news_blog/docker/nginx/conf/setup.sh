#!/usr/bin/env bash

# enable xdebug
echo 'xdebug.remote_enable=1' >> /etc/php/7.0/mods-available/xdebug.ini
echo 'xdebug.remote_connect_back=1' >> /etc/php/7.0/mods-available/xdebug.ini
echo 'xdebug.show_error_trace=1' >> /etc/php/7.0/mods-available/xdebug.ini
echo 'xdebug.remote_port=9000' >> /etc/php/7.0/mods-available/xdebug.ini
echo 'xdebug.scream=0' >> /etc/php/7.0/mods-available/xdebug.ini
echo 'xdebug.show_local_vars=1' >> /etc/php/7.0/mods-available/xdebug.ini
echo 'xdebug.idekey=PHPSTORM' >> /etc/php/7.0/mods-available/xdebug.ini

sed -i "s/;date.timezone =*/date.timezone = Europe\/Moscow/" /etc/php/7.0/fpm/php.ini
sed -i "s/;date.timezone =*/date.timezone = Europe\/Moscow/" /etc/php/7.0/cli/php.ini

# create run directories
mkdir -p /var/run/php
chown -R www-data:www-data /var/run/php