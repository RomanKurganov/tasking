@extends('layout.main')

@section('content')
        <div class="row form-add">
            <div class="col m12">
                <div class="card">
                    <div class="card-panel blue-grey lighten-4 head-title">
                        <span ><i class="material-icons left">{{$form['icon']}}</i>{{$form['title']}}</span>
                    </div>
                    @section('form')
                        <div class="row">
                            <div class="input-field col s12">
                                @include('common.elements.fields', [
                                'field' => $form['fields']['title_news']
                            ])
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                @include('common.elements.fields', [
                                'field' => $form['fields']['text_news']
                            ])
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                @include('common.elements.fields', [
                                'field' => $form['fields']['tags']
                            ])
                            </div>
                        </div>
                        @endsection
                    @include('common.form.form', [
                    'form' => $form,
                    ])

                </div>
            </div>
        </div>
@endsection