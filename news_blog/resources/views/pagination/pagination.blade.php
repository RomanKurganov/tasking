@if ($paginator->hasPages())
<ul class="pagination right">
    @if ($paginator->onFirstPage())
        <li class="disabled"><i class="material-icons">chevron_left</i></li>
    @else
        <li><a href="{{ $paginator->previousPageUrl() }}" rel="prev"><i class="material-icons">chevron_left</i></a></li>
    @endif

        @foreach ($elements as $element)

            @if (is_string($element))
                <li class="disabled"><a>{{ $element }}</a></li>
            @endif

            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                            <li class="active"><a>{{ $page }}</a></li>
                    @else
                            <li class="waves-effect"><a href="{{ $url }}">{{ $page }}</a></li>
                    @endif
                @endforeach
            @endif
        @endforeach
    @if ($paginator->hasMorePages())
        <li class="waves-effect"><a href="{{ $paginator->nextPageUrl() }}" rel="next"><i class="material-icons">chevron_right</i></a></li>

    @else
        <li class="disabled"><i class="material-icons">chevron_right</i></li>
    @endif
</ul>
@endif
<script>
    $('.pagination a').click(function(e) {
        e.preventDefault();
        var page = $(this).attr('href').split('page=')[1];
        console.log(page);
        $.ajax({
            url: '{{Request::url()}}?page=' + page,
            dataType: 'json',
            success: function (res, st, response) {
                $('#table').replaceWith(response.responseJSON);
            }
        })
    });
</script>

