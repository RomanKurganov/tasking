@if ($field['type'] === 'text')
        <input id ="{{$field['id']}}" type="text" class="validate" value="{{ isset($field['value']) ? $field['value'] : '' }}" name="{{$field['name']}}">
        <label>{{$field['title']}}</label>
@elseif ($field['type'] === 'textarea')
        <textarea id ="{{$field['id']}}" class="validate materialize-textarea"  name="{{$field['name']}}">{{ isset($field['value']) ? $field['value'] : '' }}</textarea>
        <label>{{$field['title']}}</label>

@elseif ($field['type'] === 'multiple')
        <select multiple name="{{ $field['name'] }}[]">
            <option value="" disabled selected>Выберите записи</option>
            @foreach ($field['items'] as $id => $value)
                <option value="{{ $id }}"{{ in_array($id, $field['value']) ?' selected':'' }}>{{ $value }}</option>
            @endforeach
        </select>
        <label>{{$field['title']}}</label>
        <script>
            $(document).ready(function() {
                $('select').material_select();
            });
        </script>
@elseif($field['type'] === 'chips')
    <div class="chips-autocomplete"></div>
    <script>

        $(document).ready(function() {
            var chips = $('.chips-autocomplete');
            var values = JSON.parse('{!! json_encode($field['value'])!!}'),
                allTags = JSON.parse('{!! json_encode($field['items'])!!}'),
                dataValues = [],
                tags = {};
            $.each(values, function (value) {
                dataValues.push({tag: value});

            });
            $.each(allTags, function (index, value) {
                tags[value['title_tag']] = null;
            });


            chips.material_chip({
                placeholder: "{{$field['title']}}",
                data: dataValues,
                autocompleteOptions: {
                    data: tags,
                    limit: Infinity,
                    minLength: 1
                }
            });

            chips.on('chip.add', function(e, chip) {
                chips.after("<input type='hidden' data-id='" + chip.tag + "' name='{{$field['name']}}[]' value='" + chip.tag + "'>");

            });

            chips.on('chip.delete', function(e, chip) {
                $('[data-id='+chip.tag+']').remove();
            });
            values.forEach(function (value) {
                chips.after("<input type='hidden' data-id='" + value + "' name='{{$field['name']}}[]' value='" + value + "'>");
            });
        });
    </script>
@endif
