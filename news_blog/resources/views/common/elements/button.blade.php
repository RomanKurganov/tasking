<?php /** @var \App\View\Components\Elements\Button $button */ ?>
@if ($button->getType() == 'save')
    <button type="submit" class="{{$button->getClass()}}" href="{{$button->getAction()}}">
        @if ($button->hasIcon())
            <i class="material-icons left">{{$button->getIcon()}}</i>
        @endif
        {{$button->getText()}}</button>
    @else
<a class="{{$button->getClass()}}" href="{{$button->getAction()}}">
    @if ($button->hasIcon())
        <i class="material-icons left">{{$button->getIcon()}}</i>
    @endif
{{$button->getText()}}</a>
    @endif
