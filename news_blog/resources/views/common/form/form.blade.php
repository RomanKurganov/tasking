<form action="{{ url($form['action']) }}" method="post" enctype="multipart/form-data">
    <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
    @yield('form')
    <div class="card-action ">
        <?php /** @var \App\View\Components\Elements\Button $submitButton*/ ?>
        @foreach($form['submitButton'] as $button)
            {!! $button->getViewButton() !!}
        @endforeach
    </div>
</form>
