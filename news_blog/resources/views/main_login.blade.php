@extends('layout.source')
@section('body')
<body class="login">
    <div class="container">
        <div class="row">
            <div class="login-box col s12 m12 l6 xl4 offset-l3 offset-xl4">
                @yield('content')
            </div>
        </div>
    </div>
@endsection
